package mego

import (
	"bytes"
	"errors"
	"fmt"
	"html/template"
	"io"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"sync"
)

type tplEngine struct {
	viewExt  string
	funcMap  template.FuncMap
	lock     sync.RWMutex
	viewMap  map[string]*tplCache
	compiled bool
	reader   TplReader
}

// AddFunc add new view func to the view files
func (engine *tplEngine) AddFunc(name string, viewFunc interface{}) {
	if len(name) < 1 || viewFunc == nil {
		return
	}
	if engine.funcMap == nil {
		engine.funcMap = make(template.FuncMap)
	}
	if f, ok := engine.funcMap[name]; !ok || f == nil {
		engine.funcMap[name] = viewFunc
	}
}

func (engine *tplEngine) addView(name string, v *tplCache) {
	if len(name) == 0 || v == nil {
		return
	}
	if engine.viewMap == nil {
		engine.viewMap = map[string]*tplCache{}
	}
	engine.viewMap[name] = v
}

func (engine *tplEngine) getView(name string) (*template.Template, error) {
	if engine.viewMap == nil || engine.reader == nil {
		return nil, nil
	}
	if strings.HasPrefix(name, "/") {
		name = strings.TrimLeft(name, "/")
	}
	cacheView := engine.viewMap[name]
	if cacheView == nil {
		return nil, nil
	}
	return cacheView.tpl, cacheView.err
}

func (engine *tplEngine) getDeep(file, parent string, t *template.Template) (*template.Template, [][]string, error) {
	if engine.reader == nil {
		return nil, nil, errors.New("tplEngine.reader is nil")
	}
	var fileAbsPath string

	if strings.HasPrefix(file, "../") || strings.HasPrefix(file, "./") {
		fileAbsPath = filepath.Join(filepath.Dir(parent), file)
		fileAbsPath = strings.Replace(fileAbsPath, "\\", "/", -1)
	} else if strings.HasPrefix(file, "/") {
		fileAbsPath = strings.TrimLeft(file, "/")
	} else {
		fileAbsPath = file
	}
	data, err := engine.reader.ReadFile(fileAbsPath)
	if err != nil {
		return nil, [][]string{}, err
	}
	t, err = t.New(file).Parse(string(data))
	if err != nil {
		return nil, [][]string{}, err
	}
	reg := regexp.MustCompile("[{]{2}[ \t]*template[ \t]+\"([^\"]+)\"")
	allSub := reg.FindAllStringSubmatch(string(data), -1)
	for _, m := range allSub {
		if len(m) == 2 {
			name := m[1]
			if !strings.HasSuffix(strings.ToLower(name), engine.viewExt) {
				continue
			}
			look := t.Lookup(name)
			if look != nil {
				continue
			}
			t, _, err = engine.getDeep(name, file, t)
			if err != nil {
				return nil, [][]string{}, err
			}
		}
	}
	return t, allSub, nil
}

func (engine *tplEngine) getLoop(temp *template.Template, subMods [][]string, others ...string) (t *template.Template, err error) {
	t = temp
	for _, m := range subMods {
		if len(m) == 2 {
			tpl := t.Lookup(m[1])
			if tpl != nil {
				continue
			}
			//check filename
			for _, otherFile := range others {
				if otherFile == m[1] {
					var subMods1 [][]string
					t, subMods1, err = engine.getDeep(otherFile, "", t)
					if err != nil {
						return nil, err
					} else if subMods1 != nil && len(subMods1) > 0 {
						t, err = engine.getLoop(t, subMods1, others...)
					}
					break
				}
			}
		}
	}
	return
}

func (engine *tplEngine) getTplCache(file string, others ...string) *tplCache {
	t := template.New(file)
	if engine.funcMap != nil {
		t.Funcs(engine.funcMap)
	}
	var subMods [][]string
	t, subMods, err := engine.getDeep(file, "", t)
	if err != nil {
		return &tplCache{nil, err}
	}
	t, err = engine.getLoop(t, subMods, others...)
	if err != nil {
		return &tplCache{nil, err}
	}
	return &tplCache{t, nil}
}

func (engine *tplEngine) compile() error {
	engine.lock.Lock()
	defer engine.lock.Unlock()
	if engine.compiled {
		return nil
	}
	engine.AddFunc("include", engine.includeView)
	vf := &tplFile{
		files:   make(map[string][]string),
		viewExt: engine.viewExt,
	}
	err := engine.reader.Walk(func(path string, f os.FileInfo, err error) error {
		return vf.visit(path, f, err)
	})
	if err != nil {
		return err
	}
	for _, v := range vf.files {
		for _, file := range v {
			v := engine.getTplCache(file, v...)
			engine.addView(file, v)
		}
	}
	engine.compiled = true
	return nil
}

func (engine *tplEngine) includeView(viewName string, data interface{}) template.HTML {
	str, err := engine.RenderStr(viewName, data)
	if err != nil {
		panic(err)
	}
	return template.HTML(str)
}

// Clear clear all the cache in the view engine. and re-compile the view files into memory at next call 'render' or 'RenderStr'
func (engine *tplEngine) Clear() {
	engine.lock.Lock()
	defer engine.lock.Unlock()

	engine.compiled = false
	engine.viewMap = nil
}

// render render the view tplFile with given data and then write the result to an io original.
// viewPath: the relative view tplFile path that will be rendered.
// viewData: the view data
// original: the given io original
func (engine *tplEngine) Render(writer io.Writer, viewPath string, viewData interface{}) error {
	if writer == nil {
		return errors.New("invalid original")
	}
	if len(viewPath) < 1 {
		return errors.New("invalid viewPath")
	}
	if !strings.HasSuffix(viewPath, engine.viewExt) {
		viewPath = viewPath + engine.viewExt
	}
	if !engine.compiled {
		err := engine.compile()
		if err != nil {
			return err
		}
	}
	tpl, err := engine.getView(viewPath)
	if err != nil {
		return err
	}
	if tpl == nil {
		return fmt.Errorf("the view tplFile '%s' cannot be found", viewPath)
	}
	buf := &bytes.Buffer{}
	err = tpl.Execute(buf, viewData)
	if err != nil {
		return err
	}
	_, err = io.Copy(writer, buf)
	if err != nil {
		return err
	}
	return nil
}

// RenderStr render the view tplFile with given data then get the result.
// viewPath: the relative view tplFile path that will be rendered
// viewData: the view data
func (engine *tplEngine) RenderStr(viewPath string, viewData interface{}) (string, error) {
	buf := &bytes.Buffer{}
	err := engine.Render(buf, viewPath, viewData)
	if err != nil {
		return "", err
	}
	return buf.String(), nil
}

// newTplEngine create a new view engine.
// rootDir: the root dir of the view files;
// ext: the view tplFile extension(starts with "."), and the default tplFile extension is '.gohtml';
func newTplEngine(reader TplReader, ext string) (*tplEngine, error) {
	if len(ext) == 0 {
		ext = ".gohtml"
	}
	engine := &tplEngine{
		viewExt: strings.ToLower(ext),
		reader:  reader,
	}
	return engine, nil
}
