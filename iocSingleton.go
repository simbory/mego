package mego

import "reflect"

type iocSingleton struct {
	refType  reflect.Type
	instance reflect.Value
}

func (single *iocSingleton) valueType() reflect.Type {
	return single.refType
}

func (single *iocSingleton) typeName() string {
	return single.refType.Elem().PkgPath() + "." + single.refType.Elem().Name()
}

func (single *iocSingleton) getValue(refType reflect.Type, ctx *map[reflect.Type]reflect.Value) reflect.Value {
	(*ctx)[refType] = single.instance
	return single.instance
}

func newSingleton(i interface{}, resolver valueResolver) (iocService, error) {
	notNil("i", i)
	value := resolveValue(i)
	refType, err := resolveType(i)
	if err != nil {
		return nil, err
	}
	service := &iocSingleton{
		refType:  refType,
		instance: reflect.ValueOf(i),
	}
	elem := refType.Elem()
	for i := 0; i < elem.NumField(); i++ {
		f := elem.Field(i)
		if configPath, ok := f.Tag.Lookup("config"); ok && canSetConfigValue(f.Type) && len(configPath) > 0 {
			value.FieldByName(f.Name).Set(convertValue(resolver.getConfig(configPath), f.Type))
		}
	}
	return service, nil
}
