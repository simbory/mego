package mego

import (
	"log"
	"reflect"
)

type iocContainer struct {
	services    map[reflect.Type]iocService
	controllers map[string]*iocController
	settingMap  map[string]string
}

func (c *iocContainer) getConfig(configPath string) string {
	return c.settingMap[configPath]
}

func (c *iocContainer) getValue(refType reflect.Type, ctx *map[reflect.Type]reflect.Value) reflect.Value {
	for svcType, service := range c.services {
		if svcType.AssignableTo(refType) {
			return service.getValue(refType, ctx)
		}
	}
	return nilValue
}

func (c *iocContainer) addSingleton(i interface{}) {
	service, err := newSingleton(i, c)
	panicErr(err)
	c.services[service.valueType()] = service
	log.Println("INFO: loading singleton service", service.typeName())
}

func (c *iocContainer) addTransient(i interface{}) {
	service, err := newTransient(i, c)
	panicErr(err)
	c.services[service.valueType()] = service
	log.Println("INFO: loading transient service", service.typeName())
}

func (c *iocContainer) addController(i interface{}, name string, cfg []*ActionDef) {
	service, err := newController(i, name, c, cfg)
	panicErr(err)
	c.controllers[service.ctrlName] = service
}

func (c *iocContainer) findController(key string) *iocController {
	return c.controllers[key]
}

func newIocContainer(settingMap map[string]string) *iocContainer {
	return &iocContainer{
		services:    make(map[reflect.Type]iocService),
		controllers: make(map[string]*iocController),
		settingMap:  settingMap,
	}
}
