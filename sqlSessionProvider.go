package mego

import (
	"database/sql"
	"time"
)

type sqlSessionProvider struct {
	driver  string
	conn    string
	db      *sql.DB
	timeout time.Duration
}

func (prov *sqlSessionProvider) Init(timeout time.Duration) error {
	prov.timeout = timeout
	db, err := sql.Open(prov.driver, prov.conn)
	if err != nil {
		return err
	}
	prov.db = db
	return nil
}

func (prov *sqlSessionProvider) Read(sid string) SessionStore {
	store := &sqlSessionStore{
		id:        sid,
		prov:      prov,
		newRecord: false,
		timeout:   prov.timeout,
	}
	selStmt, err := prov.db.Prepare(`SELECT * FROM session_state WHERE ID=? and Expired>?`)
	if err != nil {
		panic(err)
	}
	defer selStmt.Close()

	row := selStmt.QueryRow(sid, time.Now())
	var id string
	var created, updated, expired time.Time
	var data = make([]byte, 0)
	err = row.Scan(&id, &created, &updated, &expired, &data)
	if err != nil {
		if err == sql.ErrNoRows {
			store.newRecord = true
			created = time.Now()
			updated = created
			expired = created.Add(prov.timeout)
		} else {
			panic(err)
		}
	}
	store.created = created
	store.updated = updated
	store.expired = expired
	store.init(data)
	return store
}

func (prov *sqlSessionProvider) Destroy(sid string) error {
	stmt, err := prov.db.Prepare(`DELETE FROM session_state WHERE ID=?`)
	if err != nil {
		return err
	}
	res, err := stmt.Exec(sid)
	if err != nil {
		return err
	}
	_, err = res.RowsAffected()
	return err
}

func (prov *sqlSessionProvider) GC() {
	stmt, err := prov.db.Prepare(`DELETE FROM session_state WHERE Expired<=?`)
	if err != nil {
		return
	}
	stmt.Exec(time.Now())
}

// NewSQLSessionProvider create a new sql session provider
func NewSQLSessionProvider(driver, conn string) SessionProvider {
	return &sqlSessionProvider{
		driver: driver,
		conn:   conn,
	}
}
