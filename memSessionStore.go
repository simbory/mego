package mego

import (
	"sync"
	"time"
)

// memSessionStore the memory session storage struct
type memSessionStore struct {
	sid        string                 //session id
	updateTime time.Time              //last access time
	timeout    time.Duration          //the timeout duration
	value      map[string]interface{} //session store
	lock       sync.RWMutex
	prov       *memSessionProvider
	flushed    bool
}

// Set Value to memory session
func (st *memSessionStore) Set(key string, value interface{}) error {
	st.lock.Lock()
	defer st.lock.Unlock()
	st.value[key] = value
	st.updateTime = time.Now()
	return nil
}

// Get Value from memory session by key
func (st *memSessionStore) Get(key string) interface{} {
	st.lock.RLock()
	defer st.lock.RUnlock()
	st.updateTime = time.Now()
	return st.value[key]
}

// Delete in memory session by key
func (st *memSessionStore) Delete(key string) error {
	st.lock.Lock()
	defer st.lock.Unlock()
	delete(st.value, key)
	st.updateTime = time.Now()
	return nil
}

// Release Implement method, no used.
func (st *memSessionStore) Clear() error {
	st.lock.Lock()
	defer st.lock.Unlock()
	for key := range st.value {
		delete(st.value, key)
	}
	st.updateTime = time.Now()
	return nil
}

// ID get this id of memory session store
func (st *memSessionStore) ID() string {
	return st.sid
}

// Flush store the data in the SessionStore
func (st *memSessionStore) Flush() error {
	st.lock.Lock()
	defer st.lock.Unlock()
	if st.flushed {
		return nil
	}
	st.flushed = true
	st.prov.dataMap.Store(st.sid, st)
	return nil
}

// expired check the session is expired at the given time
func (st *memSessionStore) expired(now time.Time) bool {
	return st.updateTime.Add(st.timeout).Before(now)
}
