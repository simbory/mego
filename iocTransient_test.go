package mego

import (
	"reflect"
	"testing"
)

type ServiceDef struct {
}

type testStruct struct {
	Sample ServiceDef `service`
}

func Test_newTransientService(t *testing.T) {
	type args struct {
		i        interface{}
		resolver valueResolver
	}
	tests := []struct {
		name    string
		args    args
		want    iocService
		wantErr bool
	}{
		{
			name: "create_test1",
			args: args{
				i:        testStruct{},
				resolver: nil,
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := newTransient(tt.args.i, tt.args.resolver)
			if (err != nil) != tt.wantErr {
				t.Errorf("newTransient() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("newTransient() got = %v, want %v", got, tt.want)
			}
		})
	}
}
