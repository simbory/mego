package mego

import (
	"encoding/json"
	"encoding/xml"
	"log"
	"os"
	"strings"

	"github.com/valyala/fasthttp"
)

// StaticRequestHandler the default static request handler. you can replace it by calling config.SetStaticHandler
// method
func (s *Server) StaticRequestHandler(w *fasthttp.Response, r *fasthttp.Request) interface{} {
	filePath := s.MapContentPath(Bytes2Str(r.URI().Path()))
	stat, err := os.Stat(filePath)
	if err != nil {
		if os.IsNotExist(err) {
			s.HandleStatusCode(404, w, r, nil)
		} else {
			s.HandleStatusCode(403, w, r, nil)
		}
		return nil
	}
	if !stat.IsDir() {
		panicErr(w.SendFile(filePath))
	} else {
		s.HandleStatusCode(404, w, r, nil)
	}
	return nil
}

func (s *Server) serveDynamic(ctx *HttpCtx) interface{} {
	var urlPath = strings.TrimRight(Bytes2Str(ctx.req.URI().Path()), "/")
	if len(urlPath) == 0 {
		urlPath = "/"
	}
	handler, routeData, err := s.routing.lookup(urlPath)
	panicErr(err)
	if routeData == nil {
		routeData = map[string]string{}
	}
	if handler != nil {
		ctx.routeData = routeData
		defer func() {
			if ctx.session != nil {
				err := ctx.session.Flush()
				if err != nil {
					log.Printf("failed to flush session: %s", err.Error())
				}
			}
		}()
		return handler.ProcessRequest(ctx)
	}
	return nil
}

func (s *Server) flush(w *fasthttp.Response, r *fasthttp.Request, result interface{}) {
	var err error
	switch result.(type) {
	case Result:
		result.(Result).Execute(w, r)
	case string:
		content := result.(string)
		w.Header.Set("Content-Type", "text/plain; charset=utf-8")
		_, err = w.BodyWriter().Write(Str2Bytes(content))
	case []byte:
		w.Header.Set("Content-Type", "text/plain; charset=utf-8")
		_, err = w.BodyWriter().Write(result.([]byte))
	case byte:
		w.Header.Set("Content-Type", "text/plain; charset=utf-8")
		_, err = w.BodyWriter().Write([]byte{result.(byte)})
	default:
		var cType = Bytes2Str(r.Header.Peek("Content-Type"))
		var contentBytes []byte
		var err error
		if cType == "text/xml" {
			contentBytes, err = xml.Marshal(result)
			panicErr(err)
			w.Header.Set("Content-Type", "text/xml; charset=utf-8")
		} else {
			contentBytes, err = json.Marshal(result)
			panicErr(err)
			w.Header.Set("Content-Type", "application/json; charset=utf-8")
		}
		_, err = w.BodyWriter().Write(contentBytes)
	}
	panicErr(err)
}
