package mego

import "reflect"

// ActionDef the action definition struct for controller
type ActionDef struct {
	Name     string
	FuncName string
	Methods  []string
}

type iocAction struct {
	controller *iocController
	actionName string
	httpMethod string
	refType    reflect.Type
	refMethod  reflect.Method
}

type actionMap map[string]*iocAction

// invoke invoke the controller action and get the ctx
func (action *iocAction) invoke(ctx *HttpCtx) interface{} {
	controller := action.controller.getControllerEntity()
	if controller == nilValue {
		return nil
	}
	entity := controller.Interface()
	if init, ok := entity.(initializer); ok {
		init.initialize(ctx)
	} else {
		return nil
	}
	if auth, ok := entity.(Authorize); ok {
		if !auth.IsAuthorized() {
			return auth.HandleUnauthorized()
		}
	}
	actionMethod := controller.MethodByName(action.refMethod.Name)
	return actionMethod.Call(nil)[0].Interface()
}

// findAction find the action by funcName from actions
func findAction(actions []*ActionDef, funcName string) *ActionDef {
	for _, c := range actions {
		if c.FuncName == funcName {
			return c
		}
	}
	return nil
}
