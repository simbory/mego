package mego

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

// TplReader the template reader interface for the view engine
type TplReader interface {
	ReadFile(name string) ([]byte, error)
	Walk(walkFn func(path string, f os.FileInfo, err error) error) error
}

// file system template file reader
type fsTplReader struct {
	viewDir string
}

func (f *fsTplReader) ReadFile(name string) ([]byte, error) {
	viewPath := filepath.Join(f.viewDir, name)
	return ioutil.ReadFile(viewPath)
}

func (f *fsTplReader) Walk(walkFn func(string, os.FileInfo, error) error) error {
	vDir := strings.ReplaceAll(f.viewDir, "\\", "/")
	return filepath.Walk(f.viewDir, func(p string, info os.FileInfo, err error) error {
		p = strings.ReplaceAll(p, "\\", "/")
		p = strings.Trim(p[len(vDir):], "/")
		return walkFn(p, info, err)
	})
}
