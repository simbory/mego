package mego

import (
	"reflect"
	"testing"
)

func refEqual(v1, v2 reflect.Value) bool {
	value1 := v1.Interface()
	value2 := v2.Interface()
	return reflect.DeepEqual(value1, value2)
}

func Test_convertValue(t *testing.T) {
	type args struct {
		str         string
		convertType reflect.Type
	}
	tests := []struct {
		name string
		args args
		want reflect.Value
	}{
		{
			name: "test_str",
			args: args{
				str:         "str value",
				convertType: reflect.TypeOf(""),
			},
			want: reflect.ValueOf("str value"),
		},
		{
			name: "test_int",
			args: args{
				str:         "5",
				convertType: reflect.TypeOf(int(0)),
			},
			want: reflect.ValueOf(5),
		},
		{
			name: "test_int8",
			args: args{
				str:         "5",
				convertType: reflect.TypeOf(int8(0)),
			},
			want: reflect.ValueOf(int8(5)),
		},
		{
			name: "test_int16",
			args: args{
				str:         "5",
				convertType: reflect.TypeOf(int16(0)),
			},
			want: reflect.ValueOf(int16(5)),
		},
		{
			name: "test_int32",
			args: args{
				str:         "5",
				convertType: reflect.TypeOf(int32(0)),
			},
			want: reflect.ValueOf(int32(5)),
		},
		{
			name: "test_int64",
			args: args{
				str:         "5",
				convertType: reflect.TypeOf(int64(0)),
			},
			want: reflect.ValueOf(int64(5)),
		},
		{
			name: "test_uint",
			args: args{
				str:         "5",
				convertType: reflect.TypeOf(uint(0)),
			},
			want: reflect.ValueOf(uint(5)),
		},
		{
			name: "test_uint8",
			args: args{
				str:         "5",
				convertType: reflect.TypeOf(uint8(0)),
			},
			want: reflect.ValueOf(uint8(5)),
		},
		{
			name: "test_uint16",
			args: args{
				str:         "5",
				convertType: reflect.TypeOf(uint16(0)),
			},
			want: reflect.ValueOf(uint16(5)),
		},
		{
			name: "test_uint32",
			args: args{
				str:         "5",
				convertType: reflect.TypeOf(uint32(0)),
			},
			want: reflect.ValueOf(uint32(5)),
		},
		{
			name: "test_uint64",
			args: args{
				str:         "5",
				convertType: reflect.TypeOf(uint64(0)),
			},
			want: reflect.ValueOf(uint64(5)),
		},
		{
			name: "test_float32",
			args: args{
				str:         "5.01",
				convertType: reflect.TypeOf(float32(0)),
			},
			want: reflect.ValueOf(float32(5.01)),
		},
		{
			name: "test_float64",
			args: args{
				str:         "5.01",
				convertType: reflect.TypeOf(float64(0)),
			},
			want: reflect.ValueOf(5.01),
		},
		{
			name: "test_bool",
			args: args{
				str:         "true",
				convertType: reflect.TypeOf(false),
			},
			want: reflect.ValueOf(true),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := convertValue(tt.args.str, tt.args.convertType); !refEqual(got, tt.want) {
				t.Errorf("convertValue() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_resolveType(t *testing.T) {
	type args struct {
		i interface{}
	}
	v := args{}
	tests := []struct {
		name    string
		args    args
		want    reflect.Type
		wantErr bool
	}{
		{
			name: "test_type1",
			args: args{
				i: &v,
			},
			want:    reflect.TypeOf(&args{}),
			wantErr: false,
		},
		{
			name: "test_type2",
			args: args{
				i: v,
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := resolveType(tt.args.i)
			if (err != nil) != tt.wantErr {
				t.Errorf("resolveType() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("resolveType() got = %v, want %v", got, tt.want)
			}
		})
	}
}
