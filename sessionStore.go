package mego

// SessionStore the session store interface
type SessionStore interface {
	Set(key string, value interface{}) error //set session Value
	Get(key string) interface{}              //get session Value
	Delete(key string) error                 //delete session Value
	Clear() error                            //delete all data
	ID() string                              //current session ID
	Flush() error                            //release the resource & save data to provider & return the data
}
