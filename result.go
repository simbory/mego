package mego

import (
	"bufio"
	"bytes"
	"io"
	"strings"

	"github.com/valyala/fasthttp"
)

// Result the request result interface
type Result interface {
	Execute(w *fasthttp.Response, r *fasthttp.Request)
}

// BufResult the buffered result
type BufResult struct {
	writer      *bufio.Writer
	compression bool
	ContentType string
	Encoding    string
	headers     map[string]string
	StatusCode  int
}

func (b *BufResult) makeBuf() {
	if b.writer == nil {
		b.writer = bufio.NewWriter(bytes.NewBuffer(nil))
	}
}

// Write write byte array p to the result buffer
func (b *BufResult) Write(p []byte) (n int, err error) {
	b.makeBuf()
	return b.writer.Write(p)
}

// WriteByte write byte c to the result buffer
func (b *BufResult) WriteByte(c byte) error {
	b.makeBuf()
	return b.writer.WriteByte(c)
}

// WriteString write string s to the result buffer
func (b *BufResult) WriteString(s string) (n int, err error) {
	b.makeBuf()
	return b.writer.WriteString(s)
}

// WriteRune write rune r to the result buffer
func (b *BufResult) WriteRune(r rune) (n int, err error) {
	b.makeBuf()
	return b.writer.WriteRune(r)
}

// ReadFrom read the data from reader r and write to the result buffer
func (b *BufResult) ReadFrom(r io.Reader) (n int64, err error) {
	b.makeBuf()
	return b.writer.ReadFrom(r)
}

// AddHeader write http header to the result
func (b *BufResult) AddHeader(key, value string) {
	notEmpty("key", key)
	notEmpty("value", value)
	if strings.EqualFold(key, "content-type") {
		b.ContentType = value
		return
	}
	if b.headers == nil {
		b.headers = make(map[string]string)
	}
	b.headers[key] = value
}

// Execute write the data in the result buffer to the response original
func (b *BufResult) Execute(w *fasthttp.Response, r *fasthttp.Request) {
	if len(b.headers) > 0 {
		for key, value := range b.headers {
			w.Header.Set(key, value)
		}
	}
	if len(b.Encoding) == 0 {
		b.Encoding = "utf-8"
	}
	if len(b.ContentType) == 0 {
		b.ContentType = "text/plain"
	}
	w.Header.Set("content-type", StrJoin(b.ContentType, "; charset=", b.Encoding))
	if b.StatusCode <= 0 {
		b.StatusCode = 200
	}
	w.SetStatusCode(b.StatusCode)
	if b.writer == nil {
		return
	}
	if b.compression && acceptGzip(r) {
		panicErr(w.WriteGzip(b.writer))
	} else if b.compression && acceptDeflate(r) {
		panicErr(w.WriteDeflate(b.writer))
	} else {
		panicErr(w.Write(b.writer))
	}
}

// NewBufResult create a new buffer result with default value buf
func NewBufResult(buf *bytes.Buffer, compression bool) *BufResult {
	return &BufResult{writer: bufio.NewWriter(buf)}
}

// EmptyResult the empty buffer result
type EmptyResult struct{}

// Execute do nothing
func (er *EmptyResult) Execute(w *fasthttp.Response, r *fasthttp.Request) {
}

// FileResult the file result
type FileResult struct {
	ContentType string
	FilePath    string
}

// Execute execute the result
func (fr *FileResult) Execute(w *fasthttp.Response, r *fasthttp.Request) {
	if len(fr.ContentType) > 0 {
		w.Header.Set("Content-Type", fr.ContentType)
	}
	panicErr(w.SendFile(fr.FilePath))
}

// RedirectResult the redirect result
type RedirectResult struct {
	RedirectURL string
	StatusCode  int
}

// Execute execute the redirect result
func (rr *RedirectResult) Execute(w *fasthttp.Response, r *fasthttp.Request) {
	var statusCode = 301
	if rr.StatusCode != 301 {
		statusCode = 302
	}
	w.SetStatusCode(statusCode)
	w.Header.Set("Location", rr.RedirectURL)
	panic(&endCtxSignal{})
}
