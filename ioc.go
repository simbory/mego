package mego

import (
	"errors"
	"reflect"
	"strconv"
)

type valueResolver interface {
	getConfig(configPath string) string
	getValue(refType reflect.Type, ctx *map[reflect.Type]reflect.Value) reflect.Value
}

type iocService interface {
	valueType() reflect.Type
	getValue(refType reflect.Type, ctx *map[reflect.Type]reflect.Value) reflect.Value
	typeName() string
}

var nilValue = reflect.Value{}
var iocDefError = errors.New("the ioc injection field definition should be the interface type, such as *Struct")
var strType = reflect.TypeOf("")
var intType = reflect.TypeOf(int(0))
var int8Type = reflect.TypeOf(int8(0))
var int16Type = reflect.TypeOf(int16(0))
var int32Type = reflect.TypeOf(int32(0))
var int64Type = reflect.TypeOf(int64(0))
var uintType = reflect.TypeOf(uint(0))
var uint8Type = reflect.TypeOf(uint8(0))
var uint16Type = reflect.TypeOf(uint16(0))
var uint32Type = reflect.TypeOf(uint32(0))
var uint64Type = reflect.TypeOf(uint64(0))
var float32Type = reflect.TypeOf(float32(0))
var float64Type = reflect.TypeOf(float64(0))
var boolType = reflect.TypeOf(false)

func resolveType(i interface{}) (v reflect.Type, err error) {
	refType := reflect.TypeOf(i)
	if refType.Kind() != reflect.Ptr || refType.Elem().Kind() != reflect.Struct {
		return nil, errors.New("invalid ioc service type. make sure the service type is &Struct{}")
	}
	return refType, nil
}

func resolveValue(i interface{}) reflect.Value {
	refValue := reflect.ValueOf(i)
	if refValue.Kind() == reflect.Ptr {
		refValue = refValue.Elem()
	}
	if refValue.Kind() != reflect.Struct {
		panic(errors.New("invalid service kind"))
	}
	return refValue
}

func canSetConfigValue(refType reflect.Type) bool {
	return strType == refType || intType == refType || int8Type == refType || int16Type == refType ||
		int32Type == refType || int64Type == refType || uintType == refType || uint8Type == refType ||
		uint16Type == refType || uint32Type == refType || uint64Type == refType || float32Type == refType ||
		float64Type == refType || boolType == refType
}

func isInterfaceType(refType reflect.Type) bool {
	return refType.Kind() == reflect.Interface ||
		(refType.Kind() == reflect.Ptr && refType.Elem().Kind() == reflect.Struct)
}

func convertValue(str string, convertType reflect.Type) reflect.Value {
	switch convertType {
	case strType:
		return reflect.ValueOf(str)
	case intType:
		if len(str) == 0 {
			return reflect.ValueOf(int(0))
		} else {
			num, err := strconv.ParseInt(str, 10, 32)
			panicErr(err)
			return reflect.ValueOf(int(num))
		}
	case int8Type:
		if len(str) == 0 {
			return reflect.ValueOf(int8(0))
		} else {
			num, err := strconv.ParseInt(str, 10, 8)
			panicErr(err)
			return reflect.ValueOf(int8(num))
		}
	case int16Type:
		if len(str) == 0 {
			return reflect.ValueOf(int16(0))
		} else {
			num, err := strconv.ParseInt(str, 10, 16)
			panicErr(err)
			return reflect.ValueOf(int16(num))
		}
	case int32Type:
		if len(str) == 0 {
			return reflect.ValueOf(int32(0))
		} else {
			num, err := strconv.ParseInt(str, 10, 32)
			panicErr(err)
			return reflect.ValueOf(int32(num))
		}
	case int64Type:
		if len(str) == 0 {
			return reflect.ValueOf(int64(0))
		} else {
			num, err := strconv.ParseInt(str, 10, 64)
			panicErr(err)
			return reflect.ValueOf(int64(num))
		}
	case uintType:
		if len(str) == 0 {
			return reflect.ValueOf(uint(0))
		} else {
			num, err := strconv.ParseUint(str, 10, 32)
			panicErr(err)
			return reflect.ValueOf(uint(num))
		}
	case uint8Type:
		if len(str) == 0 {
			return reflect.ValueOf(uint8(0))
		} else {
			num, err := strconv.ParseUint(str, 10, 8)
			panicErr(err)
			return reflect.ValueOf(uint8(num))
		}
	case uint16Type:
		if len(str) == 0 {
			return reflect.ValueOf(uint16(0))
		} else {
			num, err := strconv.ParseUint(str, 10, 16)
			panicErr(err)
			return reflect.ValueOf(uint16(num))
		}
	case uint32Type:
		if len(str) == 0 {
			return reflect.ValueOf(uint32(0))
		} else {
			num, err := strconv.ParseUint(str, 10, 32)
			panicErr(err)
			return reflect.ValueOf(uint32(num))
		}
	case uint64Type:
		if len(str) == 0 {
			return reflect.ValueOf(uint64(0))
		} else {
			num, err := strconv.ParseUint(str, 10, 64)
			panicErr(err)
			return reflect.ValueOf(uint64(num))
		}
	case float32Type:
		if len(str) == 0 {
			return reflect.ValueOf(float32(0))
		} else {
			num, err := strconv.ParseFloat(str, 10)
			panicErr(err)
			return reflect.ValueOf(float32(num))
		}
	case float64Type:
		if len(str) == 0 {
			return reflect.ValueOf(float64(0))
		} else {
			num, err := strconv.ParseFloat(str, 10)
			panicErr(err)
			return reflect.ValueOf(num)
		}
	case boolType:
		if len(str) == 0 {
			return reflect.ValueOf(false)
		} else {
			b, err := strconv.ParseBool(str)
			panicErr(err)
			return reflect.ValueOf(b)
		}
	default:
		panic(errors.New("invalid type conversion"))
	}
}
