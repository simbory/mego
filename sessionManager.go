package mego

import (
	"encoding/gob"
	"math/rand"
	"net/url"
	"sync"
	"time"

	"github.com/valyala/fasthttp"
)

// SessionConfig the session configuration struct
type SessionConfig struct {
	// CookieName the name of the session cookie
	CookieName string `xml:"cookieName" json:"cookieName"`
	// Timeout the session cookie timeout in mili-seconds
	Timeout  int64  `xml:"timeout,attr" json:"timeout"`
	HTTPOnly bool   `xml:"httpOnly,attr" json:"http_only"`
	Secure   bool   `xml:"secure,attr" json:"secure"`
	Domain   string `xml:"domain,attr" json:"domain"`
}

// SessionManager the session manager struct
type SessionManager struct {
	provider    SessionProvider
	config      *SessionConfig
	initialized bool
	lock        sync.RWMutex
	managerID   string
}

func (manager *SessionManager) initialize() {
	// DCL: double checked locking
	if manager.initialized {
		return
	}
	manager.lock.Lock()
	defer manager.lock.Unlock()
	if manager.initialized {
		return
	}

	err := manager.provider.Init(time.Second * time.Duration(manager.config.Timeout))
	if err != nil {
		panic(err)
	}
	go manager.gc()
	manager.initialized = true
}

func (manager *SessionManager) getSessionID(r *fasthttp.Request) string {
	value := r.Header.Cookie(manager.config.CookieName)
	if len(value) == 0 {
		return ""
	}
	return Bytes2Str(value)
}

// gc Start session gc process.
// it can do gc in times after gc lifetime.
func (manager *SessionManager) gc() {
	manager.provider.GC()
	time.AfterFunc(time.Second*10, func() {
		manager.gc()
	})
}

// Start generate or read the session id from http request.
// if session id exists, return SessionStore with this id.
func (manager *SessionManager) Start(w *fasthttp.Response, r *fasthttp.Request) SessionStore {
	manager.initialize()
	id := manager.getSessionID(r)
	if id != "" {
		return manager.provider.Read(id)
	}
	// Generate a new store
	id = newGuidStr()
	store := manager.provider.Read(id)

	cookie := &fasthttp.Cookie{}
	cookie.SetKey(manager.config.CookieName)
	cookie.SetValue(id)
	cookie.SetPath("/")
	cookie.SetHTTPOnly(manager.config.HTTPOnly)
	cookie.SetSecure(manager.config.Secure)

	if len(manager.config.Domain) > 0 {
		cookie.SetDomain(manager.config.Domain)
	}
	r.Header.SetCookie(manager.config.CookieName, id)
	w.Header.SetCookie(cookie)
	return store
}

// Destroy Destroy session by its id in http request cookie.
func (manager *SessionManager) Destroy(w *fasthttp.Response, r *fasthttp.Request) {
	manager.initialize()
	value := r.Header.Cookie(manager.config.CookieName)
	if len(value) == 0 {
		return
	}
	sid, _ := url.QueryUnescape(Bytes2Str(value))
	err := manager.provider.Destroy(sid)
	if err != nil {
		panic(err)
	}
	expiration := time.Now().Add(-10000)

	cookie := &fasthttp.Cookie{}
	cookie.SetKey(manager.config.CookieName)
	cookie.SetPath("/")
	cookie.SetHTTPOnly(manager.config.HTTPOnly)
	cookie.SetSecure(manager.config.Secure)
	if len(manager.config.Domain) > 0 {
		cookie.SetDomain(manager.config.Domain)
	}
	cookie.SetExpire(expiration)
	cookie.SetMaxAge(-1)
	r.Header.SetCookie(manager.config.CookieName, "")
	w.Header.SetCookie(cookie)
}

func RegisterType(value interface{}) {
	gob.Register(value)
}

func RegisterTypeName(name string, value interface{}) {
	gob.RegisterName(name, value)
}

func newGuidStr() string {
	str := "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-$#_+="
	bytes := []byte(str)
	var result []byte
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	for i := 0; i < 32; i++ {
		result = append(result, bytes[r.Intn(len(bytes))])
	}
	return string(result)
}
