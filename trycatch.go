package mego

type CatchFunc func(ex interface{})

type TryCache interface {
	Catch(f CatchFunc) TryCache
	Finally(f func())
}

type tryCache struct {
	try   func()
	tried bool
}

func (t *tryCache) Catch(cache CatchFunc) (ret TryCache) {
	if t.try == nil || t.tried {
		return t
	}
	defer func() {
		t.tried = true
		err := recover()
		ret = t
		if err != nil && cache != nil {
			cache(err)
		}
	}()
	t.try()
	return
}

func (t *tryCache) Finally(f func()) {
	if !t.tried {
		t.Catch(nil)
	}
	if f != nil {
		f()
	}
}

func Try(try func()) TryCache {
	return &tryCache{try: try}
}
