package mego

import (
	"errors"
	"log"
	"reflect"
	"strings"
)

var httpMethods = []string{
	"Get",
	"Head",
	"Put",
	"Post",
	"Trace",
	"Options",
	"Delete",
}

var ignoredMethods = []string{
	"Session",
	"IsAuthorized",
	"HandleUnauthorized",
	"Items",
}

func getControllerName(given string, elem reflect.Type) (string, error) {
	name := given
	if len(name) == 0 {
		elemName := strings.ToLower(elem.Name())
		if len(elemName) <= 10 || !strings.HasSuffix(elemName, "controller") {
			return "", errors.New("invalid controller name")
		}
		name = strings.ToLower(elem.Name()[0 : len(elemName)-10])
	}
	return name, nil
}

// isIgnoreMethod check the name is in the ignoredMethods list
func isIgnoreMethod(name string) bool {
	for _, m := range ignoredMethods {
		if name == m {
			return true
		}
	}
	return false
}

type iocController struct {
	iocTransient
	structName string
	pkg        string
	ctrlName   string
	actions    map[string]actionMap
}

func (c *iocController) setAction(action *iocAction) {
	m := c.actions[action.actionName]
	if m == nil {
		m = make(actionMap)
	}
	m[action.httpMethod] = action
	c.actions[action.actionName] = m
	log.Printf("INFO: loading action \"%s\" for controller \"%s\", function name: \"%s\", HTTP method: %s",
		action.actionName,
		c.ctrlName,
		action.refMethod.Name,
		action.httpMethod)
}

func (c *iocController) findAction(actionName, httpMethod string) *iocAction {
	mappings := c.actions[actionName]
	if mappings == nil {
		return nil
	}
	action := mappings[httpMethod]
	if action == nil {
		action = mappings["*"]
	}
	return action
}

func (c *iocController) initActions(refType, elem reflect.Type, cfg []*ActionDef) {
	for i := 0; i < refType.NumMethod(); i++ {
		method := refType.Method(i)
		numIn := method.Type.NumIn()
		numOut := method.Type.NumOut()
		if numIn != 1 || numOut != 1 {
			continue
		}
		outKind := method.Type.Out(0).Kind()
		if len(cfg) > 0 {
			cfg := findAction(cfg, method.Name)
			if cfg != nil {
				if len(cfg.Methods) < 1 {
					c.setAction(&iocAction{
						actionName: cfg.Name,
						httpMethod: "*",
						refType:    elem,
						refMethod:  method,
						controller: c,
					})
				} else {
					for _, m := range cfg.Methods {
						c.setAction(&iocAction{
							actionName: cfg.Name,
							httpMethod: strings.ToUpper(m),
							refType:    elem,
							refMethod:  method,
							controller: c,
						})
					}
				}
			}
		} else {
			if outKind != reflect.Interface || isIgnoreMethod(method.Name) {
				continue
			}
			action := &iocAction{
				refType:    elem,
				refMethod:  method,
				controller: c,
			}
			var actionName string
			var httpMethod string
			for _, m := range httpMethods {
				if strings.HasPrefix(method.Name, m) {
					actionName = strings.ToLower(method.Name[len(m):len(method.Name)])
					httpMethod = strings.ToUpper(m)
					break
				}
			}
			if len(actionName) == 0 {
				if len(httpMethod) == 0 {
					actionName = strings.ToLower(method.Name)
					httpMethod = "*"
				} else {
					continue
				}
			}
			action.actionName = actionName
			action.httpMethod = httpMethod
			c.setAction(action)
		}
	}
}

func (c *iocController) getControllerEntity() reflect.Value {
	iocCtx := make(map[reflect.Type]reflect.Value)
	return c.getValue(nil, &iocCtx)
}

func newController(i interface{}, name string, resolver valueResolver, cfg []*ActionDef) (*iocController, error) {
	transient, err := newTransient(i, resolver)
	if err != nil {
		return nil, err
	}
	elem := transient.refType.Elem()
	ctrlName, err := getControllerName(name, elem)
	if err != nil {
		return nil, err
	}
	log.Printf("INFO: loading controller \"%s\", type %s\r\n", ctrlName, transient.typeName())
	controller := &iocController{
		structName: elem.Name(),
		pkg:        elem.PkgPath(),
		ctrlName:   ctrlName,
		actions:    make(map[string]actionMap),
	}
	controller.iocTransient = *transient
	controller.initActions(transient.refType, elem, cfg)
	return controller, nil
}
