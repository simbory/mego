package mego

import "strings"

func any(urlPath string, opt RouteOpt) string {
	var length = len(urlPath)
	if length >= opt.MinLength() && length <= opt.MaxLength() {
		return urlPath
	}
	return ""
}

func word(urlPath string, opt RouteOpt) string {
	bytes := wordReg.Find([]byte(urlPath))
	if len(bytes) >= opt.MinLength() && len(bytes) <= opt.MaxLength() {
		return string(bytes)
	}
	return ""
}

func num(urlPath string, opt RouteOpt) string {
	var numBytes []byte
	for _, char := range []byte(urlPath) {
		if isNumber(char) {
			numBytes = append(numBytes, char)
		} else {
			break
		}
		if len(numBytes) >= opt.MaxLength() {
			break
		}
	}
	if len(numBytes) >= opt.MinLength() {
		return string(numBytes)
	}
	return ""
}

func enum(urlPath string, opt RouteOpt) string {
	if len(opt.Setting()) == 0 {
		return ""
	}
	var splits = strings.Split(opt.Setting(), "|")
	for _, value := range splits {
		if strings.HasPrefix(urlPath, value) {
			return value
		}
	}
	return ""
}
