package mego

import (
	"errors"
	"fmt"
)

// panicErr if the error is not nil, panic the error
func panicErr(err error) {
	if err != nil {
		panic(err)
	}
}

// assert execute the function 'f', if the function 'f' return false, panic the error message 'msg'
func assert(f func() bool, msg string) {
	if !f() {
		panic(errors.New(msg))
	}
}

// assertArg assert that the checkFunc should be passed, or the program will panic error.
func assertArg(argName string, assertFunc func() bool) {
	assert(assertFunc, fmt.Sprintf("invalid value of parameter '%s'", argName))
}

// notNil assert the data should not be nil
func notNil(argName string, data interface{}) {
	assert(func() bool {
		return data != nil
	}, fmt.Sprintf("%s is nil", argName))
}

// assertArg assert the string length should not be empty
func notEmpty(argName, value string) {
	assert(func() bool {
		return len(value) > 0
	}, fmt.Sprintf("%s is empty", argName))
}
