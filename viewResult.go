package mego

import (
	"bufio"
	"bytes"
	"github.com/valyala/fasthttp"
)

// viewResult the view result struct
type viewResult struct {
	viewName    string
	data        interface{}
	engine      *tplEngine
	compression bool
}

// Execute execute the view and write the view result to the response original
func (vr *viewResult) Execute(w *fasthttp.Response, r *fasthttp.Request) {
	w.Header.Set("Content-Type", "text/html; charset=utf-8")
	if vr.compression && acceptGzip(r) {
		writer := bufio.NewWriter(&bytes.Buffer{})
		err := vr.engine.Render(writer, vr.viewName, vr.data)
		panicErr(err)
		panicErr(w.WriteGzip(writer))
	} else if vr.compression && acceptDeflate(r) {
		writer := bufio.NewWriter(&bytes.Buffer{})
		err := vr.engine.Render(writer, vr.viewName, vr.data)
		panicErr(err)
		panicErr(w.WriteDeflate(writer))
	} else {
		err := vr.engine.Render(w.BodyWriter(), vr.viewName, vr.data)
		panicErr(err)
	}
}
