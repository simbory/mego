package main

import (
	"bytes"
	"flag"
	"fmt"
	"os"

	"gitee.com/simbory/mego/cmd/mego/code"
	"gitee.com/simbory/mego/cmd/mego/proj"
	"gitee.com/simbory/mego/cmd/mego/version"
)

type MegoCmd interface {
	CmdName() string
	Desc() string
	Exec(args []string)
}

var commands = map[string]MegoCmd{}

func addMegoCmd(cmd MegoCmd) {
	commands[cmd.CmdName()] = cmd
}

func init() {
	addMegoCmd(&proj.Generator{})
	addMegoCmd(&code.Generator{})
	addMegoCmd(&version.Printer{})
}

func main() {
	flag.Parse()
	args := flag.Args()
	if len(args) == 0 {
		printUsages()
	}
	cmdName := args[0]
	cmd := commands[cmdName]
	if cmd != nil {
		os.Args = args
		cmd.Exec(args[1:])
	} else {
		printUsages()
	}
}

const defaultUsage = `mego is a powerful and easy-to-use web framework which supports MVC and IOC
Usage:
    mego <command> [arguments]
The commands are:`

func printUsages() {
	fmt.Println(defaultUsage)
	maxLength := cmdNameMaxLen()
	for cmdName, megoCmd := range commands {
		fmt.Println(fixCmdNamePrint(cmdName, maxLength) + megoCmd.Desc())
	}
	os.Exit(0)
}

func cmdNameMaxLen() int {
	length := 0
	for cmdName, _ := range commands {
		if len(cmdName) > length {
			length = len(cmdName)
		}
	}
	return length
}

func fixCmdNamePrint(cmdName string, maxLength int) string {
	totalLength := (maxLength/4 + 2) * 4
	buf := &bytes.Buffer{}
	buf.WriteString("    ")
	buf.WriteString(cmdName)
	for i := len(cmdName); i < totalLength; i++ {
		buf.WriteString(" ")
	}
	return buf.String()
}
