package common

import (
	"bytes"
	"errors"
	"fmt"
	"regexp"
	"strings"
)

type AnnotationMap map[string]string

func (ann AnnotationMap) ContainsKey(key string) bool {
	_, ok := ann[key]
	return ok
}

func (ann AnnotationMap) String() string {
	sb := &bytes.Buffer{}
	for key, value := range ann {
		if sb.Len() > 0 {
			sb.WriteString(", ")
		}
		if len(value) == 0 {
			sb.WriteString(key)
		} else {
			sb.WriteString(fmt.Sprintf("%s=%s", key, value))
		}
	}
	return "// " + sb.String()
}

func ParseAnnotation(line string) (result AnnotationMap, err error) {
	reg1 := regexp.MustCompile("^//[\t ]*@.+")
	reg2 := regexp.MustCompile("^@[a-zA-Z][a-zA-Z0-9_]*(=.+)?$")
	if !reg1.MatchString(line) {
		return nil, nil
	}
	line = strings.Trim(line, "/\t ")
	lines := strings.Split(line, ",")
	result = make(AnnotationMap)
	for _, annotation := range lines {
		annotation = strings.TrimSpace(annotation)
		if !reg2.MatchString(annotation) {
			return nil, errors.New("invalid annotation: " + annotation)
		}
		eqIndex := strings.Index(annotation, "=")
		if eqIndex > 0 {
			result[annotation[0:eqIndex]] = annotation[eqIndex+1:]
		} else {
			result[annotation] = ""
		}
	}
	return
}
