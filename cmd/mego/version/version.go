package version

import (
	"fmt"
	"gitee.com/simbory/mego"
)

type Printer struct{}

func (v *Printer) Desc() string {
	return "print the mego version"
}

func (v *Printer) CmdName() string {
	return "version"
}

func (v *Printer) Exec(args []string) {
	if len(args) > 0 {
		fmt.Println("invalid command. the command \"mego version\" has no arguments")
		return
	}
	fmt.Println(mego.Version)
}
