package proj

import (
	"bytes"
	"fmt"
	"html/template"
	"io/ioutil"
	"os"
	"path"
	"strings"
)

type Generator struct{}

func (gen *Generator) Desc() string {
	return "create a new mego mvc project"
}

func (gen *Generator) CmdName() string {
	return "new"
}

func (gen *Generator) Exec(args []string) {
	if len(args) != 1 {
		fmt.Println("mego " + gen.CmdName() + ": " + gen.Desc())
		fmt.Println("Usage:\r\n  mego new [project-name]")
		os.Exit(1)
	}
	projectName := path.Clean(args[0])
	err := os.MkdirAll(projectName, 0766)
	if err != nil {
		panic(err)
	}
	proj := getDirName(projectName)
	var results = make(map[string][]byte)
	for fileName, content := range files {
		results[fileName] = render(proj, content)
	}
	if stat, err := os.Stat(projectName); err != nil {
		fmt.Println("creating directory " + projectName)
		err := os.MkdirAll(projectName, 0766)
		if err != nil {
			panic(err)
		}
	} else if !stat.IsDir() {
		fmt.Println(projectName + " is not a directory")
		os.Exit(1)
	} else {
		dir, _ := ioutil.ReadDir(projectName)
		if len(dir) > 0 {
			fmt.Println(projectName + " is not an empty directory.")
			os.Exit(1)
		}
		os.MkdirAll(projectName+"/controller", 0766)
		os.MkdirAll(projectName+"/service", 0766)
		os.MkdirAll(projectName+"/views/home", 0766)
		os.MkdirAll(projectName+"/www", 0766)
	}
	for file, data := range results {
		filePath := projectName + "/" + file
		fmt.Println("writing content to file " + filePath)
		err := ioutil.WriteFile(filePath, data, 0766)
		if err != nil {
			fmt.Println(err.Error())
		}
	}
	fmt.Println("The project \"" + projectName + "\" has been created.")
}

func getDirName(projectName string) string {
	projectName = strings.ReplaceAll(projectName, "\\", "/")
	splits := strings.Split(projectName, "/")
	return splits[len(splits)-1]
}

func render(proj, content string) []byte {
	var renderObj = map[string]interface{}{
		"proj": proj,
	}
	tpl := template.New("template")
	tpl, err := tpl.Parse(content)
	if err != nil {
		panic(err)
	}
	buf := &bytes.Buffer{}
	err = tpl.Execute(buf, renderObj)
	if err != nil {
		panic(err)
	}
	return buf.Bytes()
}
