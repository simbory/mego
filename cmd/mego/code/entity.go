package code

import (
	"bytes"
	"html/template"
)

type codeInfo struct {
	Name     string
	Struct   string
	FileName string
	Line     int
}

type actionInfo struct {
	codeInfo

	Func        string
	Route       string
	HttpMethods []string
}

func (a *actionInfo) Methods() template.HTML {
	buf := &bytes.Buffer{}
	buf.WriteString("[]string{")
	if len(a.HttpMethods) > 0 {
		for i, m := range a.HttpMethods {
			if i > 0 {
				buf.WriteString(", ")
			}
			buf.WriteString("\"" + m + "\"")
		}
	}
	buf.WriteString("}")
	return template.HTML(buf.String())
}

type controllerInfo struct {
	codeInfo
	Actions []*actionInfo
}

type serviceInfo struct {
	codeInfo
	IsSingleton bool
}
