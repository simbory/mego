package mego

import (
	"bytes"
	"runtime/debug"
	"strings"

	"github.com/valyala/fasthttp"
)

// ErrHandler define the internal s error handler func
type ErrHandler func(*fasthttp.Response, *fasthttp.Request, interface{})

type errHandlerMap map[int]ErrHandler

func (m errHandlerMap) handleCode(statusCode int, w *fasthttp.Response, r *fasthttp.Request, data interface{}) {
	handler := m[statusCode]
	if handler == nil {
		handler = handle404
	}
	handler(w, r, data)
}

// handle400 the default error 400 handler
func handle400(w *fasthttp.Response, r *fasthttp.Request, data interface{}) {
	buf := bytes.NewBuffer(nil)
	writeHtmlBegin(buf, "Error 400: Bad Request")
	buf.WriteString("<h3>Error 400: Bad Request</h3>")
	buf.WriteString("<p>The request sent by the client was " +
		"syntactically incorrect: <i>" + r.URI().String() + "</i></p>")
	writeHtmlEnd(buf)
	w.Header.Set("Content-Type", "text/html; charset=utf-8")
	w.SetStatusCode(400)
	_, err := w.BodyWriter().Write(buf.Bytes())
	if err != nil {
		panic(err)
	}
}

// handle403 the default error 400 handler
func handle403(w *fasthttp.Response, r *fasthttp.Request, data interface{}) {
	buf := bytes.NewBuffer(nil)
	writeHtmlBegin(buf, "Error 403: Forbidden")
	buf.WriteString("<h3>Error 403:  Forbidden</h3>")
	buf.WriteString("<p>Access to this resource on the server is denied: <i>" + r.URI().String() + "</i></p>")
	writeHtmlEnd(buf)
	w.Header.Set("Content-Type", "text/html; charset=utf-8")
	w.SetStatusCode(403)
	_, err := w.BodyWriter().Write(buf.Bytes())
	if err != nil {
		panic(err)
	}
}

// handle404 the default error 404 handler
func handle404(w *fasthttp.Response, r *fasthttp.Request, data interface{}) {
	buf := bytes.NewBuffer(nil)
	writeHtmlBegin(buf, "Error 404: Not Found")
	buf.WriteString("<h3>Error 404: Not Found</h3>")
	buf.WriteString("<p>The page you are looking for is not found: <i>" + r.URI().String() + "</i></p>")
	writeHtmlEnd(buf)
	w.Header.Set("Content-Type", "text/html; charset=utf-8")
	w.SetStatusCode(404)
	_, err := w.BodyWriter().Write(buf.Bytes())
	if err != nil {
		panic(err)
	}
}

// handle500 the default error 500 handler
func handle500(w *fasthttp.Response, r *fasthttp.Request, rec interface{}) {
	var debugStack = string(debug.Stack())
	debugStack = strings.Replace(debugStack, "<", "&lt;", -1)
	debugStack = strings.Replace(debugStack, ">", "&gt;", -1)
	buf := &bytes.Buffer{}
	writeHtmlBegin(buf, `Error 500: Internal Server Error`)
	buf.WriteString("<h3>Error 500: Internal Server Error</h3>")
	buf.Write(Str2Bytes("<pre><code>"))

	if err, ok := rec.(error); ok {
		buf.WriteString(err.Error())
		buf.WriteString("\r\n\r\n")
	}
	buf.WriteString(debugStack)
	buf.WriteString("</code></pre>")
	w.Header.Set("Content-Type", "text/html; charset=utf-8")
	w.SetStatusCode(500)
	_, err := w.BodyWriter().Write(buf.Bytes())
	if err != nil {
		panic(err)
	}
}

func writeHtmlBegin(buf *bytes.Buffer, title string) {
	buf.WriteString(`<!DOCTYPE html><html><head><title>`)
	buf.WriteString(title)
	buf.WriteString(`</title><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></head><body>`)
}

func writeHtmlEnd(buf *bytes.Buffer) {
	buf.WriteString(`</body></html>`)
}

func newErrHandlerMap() errHandlerMap {
	m := make(errHandlerMap)
	m[400] = handle400
	m[403] = handle403
	m[404] = handle404
	m[500] = handle500
	return m
}
