package mego

import (
	"bytes"
	"database/sql"
	"encoding/gob"
	"errors"
	"sync"
	"time"
)

type sqlSessionStore struct {
	id        string
	created   time.Time
	updated   time.Time
	expired   time.Time
	prov      *sqlSessionProvider
	data      map[string]interface{}
	timeout   time.Duration
	lock      sync.RWMutex
	newRecord bool
	flushed   bool
}

func (store *sqlSessionStore) init(data []byte) error {
	dataMap := map[string]interface{}{}
	if len(data) > 0 {
		buf := bytes.NewBuffer(data)
		decoder := gob.NewDecoder(buf)
		err := decoder.Decode(&dataMap)
		if err != nil {
			return err
		}
	}
	store.data = dataMap
	return nil
}

func (store *sqlSessionStore) Set(key string, value interface{}) error {
	store.lock.Lock()
	defer store.lock.Unlock()
	if len(key) == 0 {
		return errors.New("the key is nil")
	}
	store.data[key] = value
	store.updateTime()
	return nil
}

func (store *sqlSessionStore) Get(key string) interface{} {
	store.lock.Lock()
	defer store.lock.Unlock()
	store.updateTime()
	return store.data[key]
}

func (store *sqlSessionStore) Delete(key string) error {
	store.lock.Lock()
	defer store.lock.Unlock()
	store.updateTime()
	delete(store.data, key)
	return nil
}

func (store *sqlSessionStore) ID() string {
	return store.id
}

func (store *sqlSessionStore) Clear() error {
	store.lock.Lock()
	defer store.lock.Unlock()
	store.updateTime()
	for key := range store.data {
		delete(store.data, key)
	}
	return nil
}

func (store *sqlSessionStore) updateTime() {
	store.updated = time.Now()
	store.expired = store.updated.Add(store.timeout)
}

var errFlushFail = errors.New("failed to save session data into database")

func (store *sqlSessionStore) Flush() error {
	store.lock.Lock()
	defer store.lock.Unlock()
	if store.flushed {
		return nil
	}
	store.flushed = true
	buf := &bytes.Buffer{}
	if len(store.data) > 0 {
		encoder := gob.NewEncoder(buf)
		err := encoder.Encode(store.data)
		if err != nil {
			return err
		}
	}
	var stmt *sql.Stmt
	var res sql.Result
	var err error
	if store.newRecord {
		stmt, err = store.prov.db.Prepare(`INSERT INTO session_state VALUES (?,?,?,?,?)`)
		if err != nil {
			return err
		}
		res, err = stmt.Exec(store.id, store.created, store.updated, store.expired, buf.Bytes())
	} else {
		stmt, err = store.prov.db.Prepare(`UPDATE session_state SET Updated=?,Expired=?,Data=? WHERE ID=?`)
		if err != nil {
			return err
		}
		res, err = stmt.Exec(store.updated, store.expired, buf.Bytes(), store.id)
	}
	if err != nil {
		return err
	}
	count, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if count < 1 {
		return errFlushFail
	}
	return nil
}
