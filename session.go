package mego

func newSessionManager(config *SessionConfig, provider SessionProvider) *SessionManager {
	notNil("provider", provider)
	if config == nil {
		config = new(SessionConfig)
		config.HTTPOnly = true
	}
	if len(config.CookieName) == 0 {
		config.CookieName = "SESSION_ID"
	}
	if config.Timeout <= 0 {
		config.Timeout = 1200
	}

	return &SessionManager{
		provider:  provider,
		config:    config,
		managerID: newGuidStr(),
	}
}
