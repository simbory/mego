package mego

import (
	"strings"
)

type FilterChain interface {
	DoFilter(ctx *HttpCtx)
}

type Filter interface {
	Init()
	DoFilter(ctx *HttpCtx, chain FilterChain)
	Destroy()
}

type mvcFilter struct {
	server *Server
}

func (filter *mvcFilter) Init() {
}

func (filter *mvcFilter) DoFilter(ctx *HttpCtx, chain FilterChain) {
	if filter.server.gzipEnabled(ctx.req) {
		ctx.res.Header.Set("content-encoding", "gzip")
		ctx.res.Header.Set("vary", "Accept-Encoding")
	} else if filter.server.deflateEnabled(ctx.req) {
		ctx.res.Header.Set("content-encoding", "deflate")
		ctx.res.Header.Set("vary", "Accept-Encoding")
	}
	filter.server.prepareHeader(ctx.res)
	data := filter.server.serveDynamic(ctx)
	if data != nil {
		filter.server.flush(ctx.res, ctx.req, data)
	} else {
		filter.server.HandleStatusCode(404, ctx.res, ctx.req, nil)
	}
}

func (filter *mvcFilter) Destroy() {
}

type filterConfig struct {
	urlPrefix string
	filter    Filter
}

type filterChain []*filterConfig

// DoFilter run the filter chain with http request and http response
func (filters filterChain) DoFilter(ctx *HttpCtx) {
	if len(filters) == 0 {
		return
	}
	urlPath := Bytes2Str(ctx.req.URI().Path())
	i := 0
	var currentFilter Filter
	var nextChain FilterChain
	for ; i < len(filters); i++ {
		if urlPath == filters[i].urlPrefix ||
			strings.Index(urlPath, ensurePostFix(filters[i].urlPrefix, "/")) == 0 {
			currentFilter = filters[i].filter
			nextChain = filters[i+1:]
			break
		}
	}
	if currentFilter != nil {
		currentFilter.Init()
		currentFilter.DoFilter(ctx, nextChain)
		currentFilter.Destroy()
	}
}
