package mego

// Starter the mego configuration starter interface
type Starter interface {
	Config(config *Configure)
}
