package mego

import (
	"reflect"
)

type dependency struct {
	fieldName   string
	serviceName string
	refType     reflect.Type
}

type config struct {
	field      string
	configPath string
	refType    reflect.Type
}

type iocTransient struct {
	refType      reflect.Type
	constructor  reflect.Method
	dependencies []*dependency
	configs      []*config
	resolver     valueResolver
}

func (trans *iocTransient) valueType() reflect.Type {
	return trans.refType
}

func (trans *iocTransient) typeName() string {
	return trans.refType.Elem().PkgPath() + "." + trans.refType.Elem().Name()
}

func (trans *iocTransient) setDependencies(value *reflect.Value, ctx *map[reflect.Type]reflect.Value) {
	for _, dep := range trans.dependencies {
		field := (*value).Elem().FieldByName(dep.fieldName)
		if !field.IsValid() {
			continue
		}
		if depValue, ok := (*ctx)[field.Type()]; ok {
			field.Set(depValue)
		} else if trans.resolver != nil {
			if depValue = trans.resolver.getValue(field.Type(), ctx); depValue != nilValue {
				field.Set(depValue)
			}
		}
	}
}

func (trans *iocTransient) setConfigs(value *reflect.Value) {
	for _, config := range trans.configs {
		field := (*value).Elem().FieldByName(config.field)
		field.Set(convertValue(trans.resolver.getConfig(config.configPath), config.refType))
	}
}

func (trans *iocTransient) getValue(refType reflect.Type, ctx *map[reflect.Type]reflect.Value) reflect.Value {
	value := reflect.New(trans.refType.Elem())
	trans.setDependencies(&value, ctx)
	trans.setConfigs(&value)
	method := value.MethodByName("Init")
	if method.IsValid() {
		method.Call(nil)
	}
	if refType != nil {
		(*ctx)[refType] = value
	}
	return value
}

func newTransient(i interface{}, resolver valueResolver) (*iocTransient, error) {
	refType, err := resolveType(i)
	if err != nil {
		return nil, err
	}
	svc := &iocTransient{
		refType:  refType,
		resolver: resolver,
	}
	var dependencies []*dependency
	var configs []*config
	var elem = refType.Elem()
	for i := 0; i < elem.NumField(); i++ {
		f := elem.Field(i)
		if serviceName, ok := f.Tag.Lookup("service"); ok || string(f.Tag) == "service" {
			if !isInterfaceType(f.Type) {
				return nil, iocDefError
			}
			if len(serviceName) == 0 {
				serviceName = f.Name
			}
			dependencies = append(dependencies, &dependency{
				fieldName:   f.Name,
				serviceName: serviceName,
				refType:     f.Type,
			})
		} else if configPath, ok := f.Tag.Lookup("config"); ok && canSetConfigValue(f.Type) && len(configPath) > 0 {
			configs = append(configs, &config{
				field:      f.Name,
				configPath: configPath,
				refType:    f.Type,
			})
		}
	}
	svc.dependencies = dependencies
	svc.configs = configs
	return svc, nil
}
