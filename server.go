package mego

import (
	"log"
	"strings"

	"github.com/valyala/fasthttp"
)

type endCtxSignal struct{}

// Server the mego server struct
type Server struct {
	webRoot        string
	contentRoot    string
	addr           string
	routing        *routeTree
	statusHandlers errHandlerMap
	sessionMgr     *SessionManager
	iocContainer   *iocContainer
	viewEngine     *ViewEngine
	starters       []Starter
	viewsReader    TplReader
	staticHandler  HandlerFunc
	responseConfig *ResponseConfig
	filters        filterChain
	staticPrefixes []string
}

func (s *Server) init() {
	log.Println("INFO: initializing server...")
	config := s.execStarters()
	s.initWithConfiguration(config)
	log.Println("INFO: server is initialized!")
}

// execStarters execute the server starters and get the final configuration of the server
func (s *Server) execStarters() *Configure {
	config := &Configure{
		statusHandlers: newErrHandlerMap(),
		routes:         &RouteColl{},
		settingMap:     map[string]string{},
		server:         s,
		ResponseConfig: &ResponseConfig{
			CompressionEnabled: false,
			headers: map[string]string{
				"connection":                "keep-alive",
				"cache-control":             "private",
				"strict-transport-security": "max-age=172800",
			},
		},
	}
	if len(s.starters) > 0 {
		for _, starter := range s.starters {
			starter.Config(config)
		}
	}
	return config
}

// initWithConfiguration initialize the server with config
func (s *Server) initWithConfiguration(config *Configure) {
	s.iocContainer = newIocContainer(config.settingMap)
	for _, i := range config.singleton {
		s.iocContainer.addSingleton(i)
	}
	for _, i := range config.transient {
		s.iocContainer.addTransient(i)
	}
	for _, def := range config.controllers {
		s.iocContainer.addController(def.i, def.name, def.actions)
	}
	if len(config.routes.funcMap) > 0 {
		for name, fun := range config.routes.funcMap {
			s.routing.addFunc(name, fun)
		}
	}
	if len(config.routes.arr) > 0 {
		for _, setting := range config.routes.arr {
			s.routing.addRoute(setting.routePath, setting.processor)
		}
	}
	s.statusHandlers = config.statusHandlers
	s.sessionMgr = config.sessionMgr
	s.viewEngine = config.viewEngine
	s.responseConfig = config.ResponseConfig
	if config.staticHandler != nil {
		s.staticHandler = config.staticHandler
	} else {
		s.staticHandler = s.StaticRequestHandler
	}
	s.filters = config.filters
	s.filters = append(s.filters, &filterConfig{
		urlPrefix: "/",
		filter: &mvcFilter{
			server: s,
		},
	})
	s.staticPrefixes = config.staticPrefixes
}

func (s *Server) ServeHTTP(httpCtx *fasthttp.RequestCtx) {
	r := &httpCtx.Request
	w := &httpCtx.Response
	// catch the panic error
	Try(func() {
		if s.isDynamicRequest(r) {
			ctx := &HttpCtx{req: r, res: w, Server: s, Items: map[string]interface{}{}}
			s.filters.DoFilter(ctx)
		} else {
			s.staticHandler(w, r)
		}
	}).Catch(func(ex interface{}) {
		if _, ok := ex.(*endCtxSignal); ok {
			return
		}
		log.Println(ex)
		s.HandleStatusCode(500, w, r, ex)
	})
}

func (s *Server) gzipEnabled(req *fasthttp.Request) bool {
	if !s.responseConfig.CompressionEnabled {
		return false
	}
	return acceptGzip(req)
}

func (s *Server) deflateEnabled(req *fasthttp.Request) bool {
	if !s.responseConfig.CompressionEnabled {
		return false
	}
	return acceptDeflate(req)
}

func (s *Server) prepareHeader(w *fasthttp.Response) {
	for key, value := range s.responseConfig.headers {
		w.Header.Set(key, value)
	}
}

func (s *Server) isDynamicRequest(r *fasthttp.Request) bool {
	urlPath := Bytes2Str(r.URI().Path())
	for i := range s.staticPrefixes {
		if strings.HasPrefix(urlPath, s.staticPrefixes[i]) {
			return false
		}
	}
	return true
}

func (s *Server) HandleStatusCode(statusCode int, w *fasthttp.Response, r *fasthttp.Request, data interface{}) {
	s.statusHandlers.handleCode(statusCode, w, r, data)
}
