package mego

type initializer interface {
	initialize(ctx *HttpCtx)
}

type Authorize interface {
	IsAuthorized() bool
	HandleUnauthorized() interface{}
}

type mvcHandler struct {
	defaultController string
	defaultAction     string
}

func (h *mvcHandler) ProcessRequest(ctx *HttpCtx) interface{} {
	controller := ctx.RouteVar("controller")
	if len(controller) == 0 {
		controller = h.defaultController
	}
	actionName := ctx.RouteVar("action")
	if len(actionName) == 0 {
		actionName = h.defaultAction
	}
	ctrl := ctx.Server.iocContainer.findController(controller)
	if ctrl == nil {
		return nil
	}
	action := ctrl.findAction(actionName, Bytes2Str(ctx.req.Header.Method()))
	if action == nil {
		return nil
	}
	ctx.routeData["controller"] = controller
	ctx.routeData["action"] = actionName
	return action.invoke(ctx)
}
