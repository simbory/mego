package mego

import (
	"encoding/json"
	"encoding/xml"
	"errors"
	"fmt"
	"mime/multipart"
	"regexp"
	"strings"
	"sync"

	"github.com/valyala/fasthttp"
)

// HttpCtx the mego http context struct
type HttpCtx struct {
	req       *fasthttp.Request
	res       *fasthttp.Response
	routeData map[string]string
	ended     bool
	session   SessionStore
	ctxLock   sync.RWMutex

	Server *Server
	Items  map[string]interface{}
}

// Request get the mego request
func (ctx *HttpCtx) Request() *fasthttp.Request {
	return ctx.req
}

// Response get the mego response
func (ctx *HttpCtx) Response() *fasthttp.Response {
	return ctx.res
}

func (ctx *HttpCtx) Session() SessionStore {
	ctx.ctxLock.Lock()
	defer ctx.ctxLock.Unlock()
	if ctx.session == nil {
		if ctx.Server.sessionMgr == nil {
			panic(errors.New("the session manager is not configured properly"))
		}
		ctx.session = ctx.Server.sessionMgr.Start(ctx.res, ctx.req)
	}
	return ctx.session
}

// QueryVar get the value from the url query string
func (ctx *HttpCtx) QueryVar(key string) string {
	bytes := ctx.req.URI().QueryArgs().Peek(key)
	if len(bytes) == 0 {
		return ""
	}
	return Bytes2Str(bytes)
}

// FormVar get the form value from request. It's the same as ctx.Request().FormVar(key)
func (ctx *HttpCtx) FormVar(key string) string {
	contentType := Bytes2Str(ctx.req.Header.Peek("Content-Type"))
	if strings.Index(contentType, "multipart/form-data") >= 0 {
		formData, err := ctx.req.MultipartForm()
		panicErr(err)
		value := formData.Value[key]
		if len(value) > 0 {
			return value[0]
		}
		return ""
	} else {
		bytes := ctx.req.PostArgs().Peek(key)
		if len(bytes) == 0 {
			return ""
		}
		return Bytes2Str(bytes)
	}
}

// RouteString get the route parameter value as string by key
func (ctx *HttpCtx) RouteVar(key string) string {
	return ctx.routeData[key]
}

// FormFile get the post file info
func (ctx *HttpCtx) FormFile(key string) *multipart.FileHeader {
	contentType := Bytes2Str(ctx.req.Header.Peek("Content-Type"))
	if strings.Index(contentType, "multipart/form-data") >= 0 {
		formData, err := ctx.req.MultipartForm()
		panicErr(err)
		value := formData.File[key]
		if len(value) > 0 {
			return value[0]
		}
		return nil
	} else {
		return nil
	}
}

// MapRootPath Returns the physical file path that corresponds to the specified virtual path.
func (ctx *HttpCtx) MapRootPath(path string) string {
	return ctx.Server.MapRootPath(path)
}

// MapContentPath Returns the physical file path that corresponds to the specified virtual path.
func (ctx *HttpCtx) MapContentPath(urlPath string) string {
	return ctx.Server.MapContentPath(urlPath)
}

// TextResult generate the mego result as plain text
func (ctx *HttpCtx) TextResult(content, contentType string) Result {
	result := NewBufResult(nil, ctx.Server.responseConfig.CompressionEnabled)
	_, err := result.WriteString(content)
	if err != nil {
		panic(err)
	}
	result.ContentType = contentType
	return result
}

// JsonResult generate the mego result as JSON string
func (ctx *HttpCtx) JsonResult(data interface{}) Result {
	dataJSON, err := json.Marshal(data)
	panicErr(err)
	return ctx.TextResult(Bytes2Str(dataJSON), "application/json")
}

// JsonpResult generate the mego result as jsonp string
func (ctx *HttpCtx) JsonpResult(data interface{}, callback string) Result {
	reg := regexp.MustCompile("^[a-zA-Z_][a-zA-Z0-9_]*$")
	if !reg.Match(Str2Bytes(callback)) {
		panic(fmt.Errorf("invalid JSONP callback name '%s'", callback))
	}
	dataJSON, err := json.Marshal(data)
	panicErr(err)
	return ctx.TextResult(StrJoin(callback, "(", Bytes2Str(dataJSON), ");"), "text/javascript")
}

// XmlResult generate the mego result as XML string
func (ctx *HttpCtx) XmlResult(data interface{}) Result {
	xmlBytes, err := xml.Marshal(data)
	panicErr(err)
	return ctx.TextResult(Bytes2Str(xmlBytes), "text/xml")
}

// FileResult generate the mego result as file result
func (ctx *HttpCtx) FileResult(path string, contentType string) Result {
	var resp = &FileResult{
		FilePath:    path,
		ContentType: contentType,
	}
	return resp
}

func (ctx *HttpCtx) Redirect(urlStr string, permanent bool) {
	if permanent {
		ctx.res.SetStatusCode(301)
	} else {
		ctx.res.SetStatusCode(302)
	}
	ctx.res.Header.Set("Location", urlStr)
	ctx.End()
}

// Redirect get the redirect result. if the value of 'permanent' is true ,
// the status code is 301, else the status code is 302
func (ctx *HttpCtx) RedirectResult(urlStr string, permanent bool) Result {
	if permanent {
		return &RedirectResult{
			StatusCode:  301,
			RedirectURL: urlStr,
		}
	} else {
		return &RedirectResult{
			StatusCode:  302,
			RedirectURL: urlStr,
		}
	}
}

// End end the mego context and stop the rest request function
func (ctx *HttpCtx) End() {
	ctx.ended = true
	panic(&endCtxSignal{})
}
