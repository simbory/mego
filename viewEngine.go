package mego

import (
	"html/template"
)

// ViewEngine the mego view engine struct
type ViewEngine struct {
	engine      *tplEngine
	reader      TplReader
	tplFuncMap  template.FuncMap
	compression bool
}

// render render the view 'viewName' with 'data' and get the view result
func (e *ViewEngine) render(viewName string, data interface{}) Result {
	if e.reader == nil {
		return nil
	}
	if len(viewName) == 0 {
		return nil
	}
	return &viewResult{
		viewName:    viewName,
		data:        data,
		engine:      e.engine,
		compression: e.compression,
	}
}

// Extend extend the view helper functions with 'name' and 'tplFuncMap'
func (e *ViewEngine) Extend(name string, viewFunc interface{}) {
	notEmpty("name", name)
	notNil("tplFuncMap", viewFunc)
	e.engine.AddFunc(name, viewFunc)
}

// newViewEngine create a new view engine in ViewDir with file extension '.gohtml'
func newViewEngine(ioutil TplReader) *ViewEngine {
	ve, err := newTplEngine(ioutil, ".gohtml")
	panicErr(err)
	return &ViewEngine{
		engine:     ve,
		reader:     ioutil,
		tplFuncMap: make(template.FuncMap),
	}
}
