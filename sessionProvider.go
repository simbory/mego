package mego

import "time"

// SessionProvider contains global session methods and saved SessionStores.
// it can operate a SessionStore by its id.
type SessionProvider interface {
	Init(timeout time.Duration) error
	Read(sid string) SessionStore
	Destroy(sid string) error
	GC()
}
