package mego

import (
	"errors"
	"log"
	"path"

	"github.com/valyala/fasthttp"
)

type HandlerFunc func(w *fasthttp.Response, r *fasthttp.Request) interface{}

// UseStarter attach an starter to the server
func (s *Server) UseStarter(starter Starter) {
	if starter == nil {
		panic(errors.New("starter is nil"))
	}
	s.starters = append(s.starters, starter)
}

// MapRootPath Returns the physical file path that corresponds to the specified virtual path.
// @param virtualPath: the virtual path starts with
// @return the absolute file path
func (s *Server) MapRootPath(virtualPath string) string {
	if len(virtualPath) == 0 {
		return s.webRoot
	}
	return ClearFilePath(path.Join(s.webRoot, virtualPath))
}

// MapContentPath Returns the physical file path that corresponds to the specified virtual path.
// @param virtualPath: the virtual path starts with
// @return the absolute file path
func (s *Server) MapContentPath(virtualPath string) string {
	if len(virtualPath) == 0 {
		return s.contentRoot
	}
	return ClearFilePath(path.Join(s.contentRoot, virtualPath))
}

// Run run the application as http
func (s *Server) Run() {
	s.init()
	log.Printf("INFO: the server is listening on address %s\r\n", s.addr)
	err := fasthttp.ListenAndServe(s.addr, s.ServeHTTP)
	panicErr(err)
}

// RunTLS run the application as https
func (s *Server) RunTLS(certFile, keyFile string) {
	s.init()
	log.Printf("INFO: the server is listening on address %s, TLS\r\n", s.addr)
	err := fasthttp.ListenAndServeTLS(s.addr, certFile, keyFile, s.ServeHTTP)
	panicErr(err)
}

// NewServer create a new server
//
// webRoot: the root of this web server. and the content root is '${webRoot}/www'
//
// addr: the address the server is listen on
func NewServer(webRoot, addr string) *Server {
	if len(webRoot) == 0 {
		webRoot = GetExeDir()
	}
	webRoot = path.Clean(ClearFilePath(webRoot))

	if len(addr) == 0 {
		addr = ":8888"
	}

	var s = &Server{
		webRoot:     webRoot,
		contentRoot: webRoot + "/www",
		addr:        addr,
		routing:     newRouteTree(),
	}
	return s
}
