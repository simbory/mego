package mego

import (
	"mime/multipart"

	"github.com/valyala/fasthttp"
)

type Controller struct {
	ctx      *HttpCtx
	viewData map[string]interface{}

	Request  *fasthttp.Request
	Response *fasthttp.Response
	Server   *Server
	Items    map[string]interface{}
}

func (c *Controller) initialize(ctx *HttpCtx) {
	c.ctx = ctx
	c.Request = ctx.req
	c.Response = ctx.res
	c.Server = ctx.Server
	c.Items = ctx.Items
}

func (c *Controller) Session() SessionStore {
	return c.ctx.Session()
}

func (c *Controller) SetViewData(key string, data interface{}) {
	notEmpty("key", key)
	if c.viewData == nil {
		c.viewData = map[string]interface{}{}
	}
	c.viewData[key] = data
}

func (c *Controller) QueryVar(key string) string {
	return c.ctx.QueryVar(key)
}

func (c *Controller) RouteVar(key string) string {
	return c.ctx.RouteVar(key)
}

func (c *Controller) FormVar(key string) string {
	return c.ctx.FormVar(key)
}

func (c *Controller) FormFile(name string) *multipart.FileHeader {
	return c.ctx.FormFile(name)
}

func (c *Controller) MapRootPath(path string) string {
	return c.ctx.MapRootPath(path)
}

func (c *Controller) MapContentPath(path string) string {
	return c.ctx.MapContentPath(path)
}

func (c *Controller) JsonResult(data interface{}) Result {
	return c.ctx.JsonResult(data)
}

func (c *Controller) XmlResult(data interface{}) Result {
	return c.ctx.XmlResult(data)
}

func (c *Controller) TextResult(content, contentType string) Result {
	return c.ctx.TextResult(content, contentType)
}

func (c *Controller) FileResult(path, contentType string) Result {
	return c.ctx.FileResult(path, contentType)
}

func (c *Controller) View(model interface{}) Result {
	controller := c.RouteVar("controller")
	action := c.RouteVar("action")
	viewPath := controller + "/" + action
	return c.ViewPath(viewPath, model)
}

func (c *Controller) ViewPath(viewPath string, model interface{}) Result {
	if c.ctx.Server.viewEngine == nil {
		return nil
	}
	if model != nil {
		c.SetViewData("Model", model)
	}
	return c.ctx.Server.viewEngine.render(viewPath, c.viewData)
}

func (c *Controller) Redirect(url string, permanent bool) Result {
	return c.ctx.RedirectResult(url, permanent)
}
