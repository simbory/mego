package mego

import (
	"log"
	"os"
	"path"
	"path/filepath"
	"strings"
	"unsafe"

	"github.com/valyala/fasthttp"
)

// Bytes2Str convert the byte array to string quickly
func Bytes2Str(b []byte) string {
	return *(*string)(unsafe.Pointer(&b))
}

// Str2Bytes convert the string to byte array quickly
func Str2Bytes(s string) []byte {
	x := (*[2]uintptr)(unsafe.Pointer(&s))
	h := [3]uintptr{x[0], x[1], x[1]}
	return *(*[]byte)(unsafe.Pointer(&h))
}

// StrJoin combine some strings to a single string
func StrJoin(arr ...string) string {
	if len(arr) == 0 {
		return ""
	}
	return strings.Join(arr, "")
}

// ClearFilePath clear the pathStr and return the shortest path.
func ClearFilePath(pathStr string) string {
	return path.Clean(strings.Replace(pathStr, "\\", "/", -1))
}

// GetExeDir get the current directory that the executable file is located in.
func GetExeDir() string {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}
	return dir
}

// GetWD get the current working directory
func GetWD() string {
	str, err := os.Getwd()
	panicErr(err)
	return str
}

func isA2Z(c byte) bool {
	return (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z')
}

func isNumber(c byte) bool {
	return c >= '0' && c <= '9'
}

func ensurePostFix(str, postFix string) string {
	if strings.HasSuffix(str, postFix) {
		return str
	}
	return StrJoin(str, postFix)
}

func acceptGzip(req *fasthttp.Request) bool {
	acceptEncodings := strings.Split(Bytes2Str(req.Header.Peek("accept-encoding")), ",")
	for _, acceptEncoding := range acceptEncodings {
		if strings.TrimSpace(acceptEncoding) == "gzip" {
			return true
		}
	}
	return false
}

func acceptDeflate(req *fasthttp.Request) bool {
	acceptEncodings := strings.Split(Bytes2Str(req.Header.Peek("accept-encoding")), ",")
	for _, acceptEncoding := range acceptEncodings {
		if strings.TrimSpace(acceptEncoding) == "deflate" {
			return true
		}
	}
	return false
}
