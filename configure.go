package mego

import (
	"strings"
)

// Configure the mego configuration struct
type Configure struct {
	statusHandlers errHandlerMap
	sessionMgr     *SessionManager
	routes         *RouteColl
	viewEngine     *ViewEngine
	staticHandler  HandlerFunc
	controllers    []*controllerDef
	settingMap     map[string]string
	singleton      []interface{}
	transient      []interface{}
	server         *Server
	filters        filterChain
	staticPrefixes []string

	ResponseConfig *ResponseConfig
}

type controllerDef struct {
	i       interface{}
	name    string
	actions []*ActionDef
}

type ResponseConfig struct {
	// enable or disable the gzip compression in the server response. The default value is false
	CompressionEnabled bool
	headers            map[string]string
}

func (resp *ResponseConfig) SetHeader(key, value string) {
	notEmpty("key", key)
	notEmpty("value", value)
	resp.headers[key] = value
}

func (resp *ResponseConfig) RemoveHeader(key string) {
	notEmpty("key", key)
	delete(resp.headers, key)
}

// ConfigSession set the session state. conf: the session configuration; provider: the session provider
func (config *Configure) ConfigSession(conf *SessionConfig, provider SessionProvider) {
	if conf == nil {
		conf = &SessionConfig{}
	}
	if provider == nil {
		provider = NewMemSessionProvider()
	}
	config.sessionMgr = newSessionManager(conf, provider)
}

// SetConfigSettings add the config settings to server
func (config *Configure) SetConfigSettings(settingsMap map[string]string) {
	notNil("settingsMap", settingsMap)
	for k, v := range settingsMap {
		config.settingMap[k] = v
	}
}

// ResponseConfig get the Server entity
func (config *Configure) Server() *Server {
	return config.server
}

// ConfigViewReader init the view engine via the template reader
func (config *Configure) ConfigViewReader(reader TplReader) {
	notNil("reader", reader)
	config.viewEngine = newViewEngine(reader)
}

// ConfigViewDir init the view engine directory. The filesystem template reader will be used
func (config *Configure) ConfigViewDir(viewDir string) {
	config.viewEngine = newViewEngine(&fsTplReader{viewDir: viewDir})
}

// SetErrorHandler set the status code handler that will be used in the http response
func (config *Configure) SetErrorHandler(statusCode int, handler ErrHandler) {
	notNil("handler", handler)
	if handler != nil && statusCode > 0 {
		config.statusHandlers[statusCode] = handler
	}
}

// SetStaticHandler set the static files handler
func (config *Configure) SetStaticHandler(handler HandlerFunc) {
	notNil("handler", handler)
	config.staticHandler = handler
}

// RegisterController register controller definition to the web server
func (config *Configure) RegisterController(controller interface{}, actions ...*ActionDef) {
	notNil("controller", controller)
	notNil("actions", actions)
	config.controllers = append(config.controllers, &controllerDef{
		i:       controller,
		name:    "",
		actions: actions,
	})
}

// RegisterController register controller definition with name to the web server
func (config *Configure) RegisterControllerName(controller interface{}, name string, actions ...*ActionDef) {
	notNil("controller", controller)
	notNil("name", name)
	notNil("actions", actions)
	config.controllers = append(config.controllers, &controllerDef{
		i:       controller,
		name:    name,
		actions: actions,
	})
}

// AddRouteFunc add custom route handler func to the server
func (config *Configure) AddRouteFunc(name string, fun RouteFunc) {
	notEmpty("name", name)
	notNil("fun", fun)
	config.routes.addFunc(name, fun)
}

func (config *Configure) AddFilter(urlPrefix string, filter Filter) {
	notEmpty("urlPrefix", urlPrefix)
	notNil("filter", filter)
	config.filters = append(config.filters, &filterConfig{
		urlPrefix: "/" + strings.Trim(urlPrefix, "/"),
		filter:    filter,
	})
}

// RouteMvc add the mvc handler to the route
func (config *Configure) RouteMvc(routePath, defaultController, defaultAction string) {
	notEmpty("routePath", routePath)
	config.routes.mapMvc(routePath, defaultController, defaultAction)
}

// IocSingleton add a singleton object to the dependency injection container
func (config *Configure) IocSingleton(i interface{}) {
	notNil("i", i)
	config.singleton = append(config.singleton, i)
}

// IocSingleton add a transient object type to the dependency injection container
func (config *Configure) IocTransient(i interface{}) {
	notNil("i", i)
	config.transient = append(config.transient, i)
}

func (config *Configure) SetStaticPrefixes(prefixes []string) {
	config.staticPrefixes = prefixes
}
