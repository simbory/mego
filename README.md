# mego
mego is a powerful mvc framework written in go language

## Get started with mego
1. Make sure you have go 1.13+ installed.
2. Run command `go get gitee.com/simbory/mego/cmd/mego` or `go get gitee.com/simbory/mego && go install gitee.com/simbory/mego/cmd/mego` to the mego tool
3. Run command `mego new mego-demo` to create a new project
4. Run the command `cd mego-demo` to Enter in to folder, and the following run commands to compile the project
   ```shell script
   go generate controllers/starter.go
   go build
   ```
5. Start the web server by running the command `./mego-demo`. And then open the URL http://localhost:8888/ to see the result.
