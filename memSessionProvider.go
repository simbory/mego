package mego

import (
	"sync"
	"time"
)

// memSessionProvider the memory session provider struct
type memSessionProvider struct {
	dataMap        sync.Map
	sessionTimeout time.Duration
}

// Init init memory session provider
func (mem *memSessionProvider) Init(timeout time.Duration) error {
	mem.sessionTimeout = timeout
	return nil
}

// Read get memory session store by sid
func (mem *memSessionProvider) Read(sid string) SessionStore {
	var store *memSessionStore
	data, ok := mem.dataMap.Load(sid)
	now := time.Now()
	if !ok {
		store = &memSessionStore{
			sid:        sid,
			updateTime: now,
			timeout:    mem.sessionTimeout,
			value:      make(map[string]interface{}),
			prov:       mem,
		}
	} else {
		store = data.(*memSessionStore)
		if store.expired(now) {
			store = &memSessionStore{
				sid:        sid,
				updateTime: now,
				timeout:    mem.sessionTimeout,
				value:      make(map[string]interface{}),
				prov:       mem,
			}
		} else {
			store.updateTime = now
		}
	}
	return store
}

// Destroy delete session store in memory session by id
func (mem *memSessionProvider) Destroy(sid string) error {
	mem.dataMap.Delete(sid)
	return nil
}

// GC clean expired session stores in memory session
func (mem *memSessionProvider) GC() {
	now := time.Now()
	mem.dataMap.Range(func(key, data interface{}) bool {
		store := data.(*memSessionStore)
		if store.expired(now) {
			mem.dataMap.Delete(key)
		}
		return true
	})
}

// NewMemSessionProvider create a new memory session provider with default config
func NewMemSessionProvider() SessionProvider {
	return &memSessionProvider{}
}
