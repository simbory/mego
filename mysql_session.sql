/*
create session table in mysql database
*/
CREATE TABLE IF NOT EXISTS `session_state` (
  `ID` varchar(36) NOT NULL,
  `Created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Expired` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Data` longblob DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;